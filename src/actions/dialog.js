import * as c from 'constants/actions'

export function requestByUser(payload) {
  return {
    type: c.API_GET_USER_DIALOGS_REQUEST,
    payload
  }
}
export function successByUser(payload) {
  return {
    type: c.API_GET_USER_DIALOGS_SUCCESS,
    payload
  }
}
export function errorByUser(payload) {
  return {
    type: c.API_GET_USER_DIALOGS_ERROR,
    payload
  }
}
