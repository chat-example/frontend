import * as c from 'constants/actions'

export function requestLogin(payload) {
  return {
    type: c.API_USER_LOGIN_REQUEST,
    payload
  }
}
export function successLogin(payload) {
  return {
    type: c.API_USER_LOGIN_SUCCESS,
    payload
  }
}
export function errorLogin(payload) {
  return {
    type: c.API_USER_LOGIN_SUCCESS,
    payload
  }
}

export function requestLogout(data) {
  return {
    type: c.API_USER_LOGOUT_REQUEST,
    payload: data
  }
}
export function successLogout(data) {
  return {
    type: c.API_USER_LOGOUT_SUCCESS,
    payload: data
  }
}
export function errorLogout(data) {
  return {
    type: c.API_USER_LOGOUT_SUCCESS,
    payload: data
  }
}
