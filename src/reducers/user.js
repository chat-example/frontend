import * as c from 'constants/actions'

const initialState = {
  data: {},
  authenticated: false,
  fetching: false
}

export default function user(state = initialState, action) {
  switch (action.type) {
    case c.API_USER_LOGIN_REQUEST:
      return { ...state, fetching: true }
    case c.API_USER_LOGIN_SUCCESS:
      return { ...state, fetching: false, data: action.payload, authenticated: true }
    case c.API_USER_LOGIN_ERROR:
      return { ...state, fetching: false }
    default:
      return state
  }
}
