import { combineReducers } from 'redux'

import user from './user'

const rootReducer = combineReducers({
  user
  // ...your other reducers here
})

export default rootReducer
