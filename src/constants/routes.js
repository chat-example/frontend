export const INDEX = '/'
export const ANY = '*'

export const ABOUT = '/about'
export const LOGIN = '/login'
export const DIALOGS = '/dialogs'
