import { fork, all } from 'redux-saga/effects'

import watcherUser from './user'

export default function* rootSaga() {
  yield all([
    fork(watcherUser)
  ])
}
