import { ENV_PRODUCTION } from 'constants/index'

export function setupEnvironment() {
  window.isProduction = (process.env.NODE_ENV === ENV_PRODUCTION)
}
