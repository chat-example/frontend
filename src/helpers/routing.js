import { browserHistory } from 'react-router'

import * as r from 'constants/routes'

const UNPROTECTED = [
  '',
  r.INDEX,
  r.LOGIN
]

export function redirect(nextRoute) {
  browserHistory.push(nextRoute)
}

export function isUnprotected(route) {
  return UNPROTECTED.includes(route)
}
