import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Router, Route, Redirect, IndexRedirect } from 'react-router'

import { requestLogin } from 'actions/user'
import { App } from 'components/containers'
import * as rc from 'components/routes/'

import * as r from 'constants/routes'

class Routes extends React.Component {
  constructor(props) {
    super(props)

    this.initialAuth = ::this.initialAuth
  }
  shouldComponentUpdate() {
    return false
  }
  render() {
    return (
      <Router history={this.props.history}>
        <Route path={r.INDEX} component={App} >
          <IndexRedirect to={r.LOGIN} />
          <Route path={r.LOGIN} component={rc.Login} />
          <Route component={App} onEnter={this.initialAuth} >
            <Route path={r.DIALOGS} component={rc.DialogList} />
          </Route>
          <Redirect from={r.ANY} to={r.INDEX} />
        </Route>
      </Router>
    )
  }
  initialAuth() {
    if (this.props.authenticated === false) {
      this.props.requestLogin({ byToken: true })
    }
  }
}

Routes.propTypes = {
  authenticated: PropTypes.bool.isRequired,

  history: PropTypes.object.isRequired,
  requestLogin: PropTypes.func.isRequired
}

export default connect(
  state => ({
    authenticated: state.rootReducer.user.authenticated
  }),
  dispatch => ({
    requestLogin: bindActionCreators(requestLogin, dispatch)
  })
)(Routes)

