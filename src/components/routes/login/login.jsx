import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Segment, Form, Button } from 'semantic-ui-react'

import { requestLogin as login } from 'actions/user'

import styles from './login.module.scss'

class Login extends React.PureComponent {
  constructor(props) {
    super(props)

    this.submit = ::this.submit
    this.handleEmail = (e, { value }) => this.setState({ email: value })
    this.handlePassword = (e, { value }) => this.setState({ password: value })

    this.state = {
      email: 'admin@dummy.com',
      password: 'q1w2e3r4'
    }
  }
  render() {
    return (
      <Segment className={styles.container}>
        <Form>
          <Form.Input
            label='Email'
            value={this.state.email}
            onChange={this.handleEmail}
          />
          <Form.Input
            label='Password'
            type='password'
            value={this.state.password}
            onChange={this.handlePassword}
          />
          <Button
            fluid
            loading={this.props.fetching}
            onClick={this.submit}
          >Submit</Button>
        </Form>
      </Segment>
    )
  }
  submit() {
    this.props.login(this.state)
  }
}

Login.propTypes = {
  fetching: PropTypes.bool.isRequired,

  login: PropTypes.func.isRequired
}

export default connect(
  state => ({
    fetching: state.rootReducer.user.fetching
  }),
  dispatch => ({
    login: bindActionCreators(login, dispatch)
  })
)(Login)
