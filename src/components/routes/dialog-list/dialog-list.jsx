import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { List, Image } from 'semantic-ui-react'

import { requestByUser as getUserDialogs } from 'actions/dialog'

class DialogList extends React.PureComponent {
  state = {}
  componentDidMount() {
    this.props.getUserDialogs()
  }
  render() {
    return (
      <List divided verticalAlign='middle'>
        <List.Item>
          <Image avatar src='https://cdn1.iconfinder.com/data/icons/ninja-things-1/720/ninja-background-128.png' />
          <List.Content>
            <List.Header as='a'>Elliot Fu</List.Header>
          </List.Content>
        </List.Item>
      </List>
    )
  }
}

DialogList.propTypes = {
  getUserDialogs: PropTypes.func.isRequired
}

export default connect(
  () => ({}),
  dispatch => ({
    getUserDialogs: bindActionCreators(getUserDialogs, dispatch)
  })
)(DialogList)
